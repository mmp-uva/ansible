# Ansible Playbooks for MMP

NOTE: Requires Ansible 2.4!!

This repository contains the ansible playbooks used by MMP to configure and manage their computers.

Currently there are two groups over playbooks: laboratory and server.

The laboratory group contains playbooks to install/configure software like pycultivator, shiny-server and relevant apps.

While the server group contains playbooks for nginx, postgresql, redmine etc.

Most tasks are defined within roles, where each role manages one application. Playbooks in the root directory of each group will load roles according to what is needed, while roles trigger the loading of other roles via dependencies.

At some point I realised that many roles are shared between the groups, hence the roles in the root directory.

## Inventory

Not included in the git repository are the inventory files. I use two for each group: `staging.inventory` and `production.inventory`, each with the default server group `[servers]`.

The `staging.inventory` files contain the details of the Vagrant boxes, something like:

```
[servers]
lab.uva.dev ansible_host=192.168.33.10 ansible_connection=ssh ansible_ssh_user=vagrant ansible_ssh_private_key_file=.vagrant/machines/lab.uva.dev/virtualbox/private_key
```

Make sure to match them to the correct settings as defined in the Vagrant file.

The benefit of using separate inventory files is because you can easily see where you run your ansible playbooks on with minimal effort:

`ansible-playbook -i staging.inventory setup.yml`

vs

`ansible-playbook -i production.inventory setup.yml`

## Configuration

The roles are highly configurable. By default some will use standard (and STUPID) passwords like 1234! You should set them to values more appropriate. For this reason in some roles I load the "settings.yml" file (not included in the repository), this file should then contain you specific settings like application and admin passwords etc.

## Dependencies

* EPEL Playbook from http://github.com/geerlingguy/ansible-role-repo-epel (loaded as git submodule)
