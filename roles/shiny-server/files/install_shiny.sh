#!/bin/bash
#
# Installation script for shiny-server
# Taken from https://github.com/rstudio/shiny-server/wiki/Building-Shiny-Server-from-Source
#
# Author: Joeri Jongbloets <joeri@jongbloets.net>
#
# Version: 08/08/2017
# 
# Run-as: shiny

SRC_DIR="/opt/shiny-server"

cd $SRC_DIR

# remove old dirs
rm -rf ./tmp
rm -rf ./build

# create tmp dir
mkdir ./tmp
cd $SRC_DIR/tmp

DIR=`pwd`
PATH=$DIR/../bin:$PATH

PYTHON=`which python
# test version`
$PYTHON --version

# use cmake to prepare for make
cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DPYTHON="$PYTHON" ../

# recompile
make
mkdir $SRC_DIR/build

(cd $SRC_DIR && ./bin/npm --python="$PYTHON" install)
(cd $SRC_DIR && ./bin/node ./ext/node/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js --python="$PYTHON" rebuild)

# Install the software at the predefined location
sudo make install

# Done


