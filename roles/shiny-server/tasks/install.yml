---

- name: "Install shiny R package"
  become: true
  become_method: sudo
  command: Rscript --vanilla -e "install.packages('shiny', repos=c('https://cran.rstudio.com/'))"

- name: "Create shiny user"
  become: true
  become_method: sudo
  user:
    name: "{{ shiny_user }}"
    system: yes

- name: "Create Shiny Web Directory"
  become: yes
  become_method: sudo
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ shiny_user }}"
    group: "{{ www_group }}"
    recurse: true
  with_items:
    - "{{ shiny_web_dir }}"
    - "{{ shiny_web_dir }}/apps"

- name: "Create Shiny Log Directory"
  become: yes
  become_method: sudo
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ shiny_user }}"
    group: "{{ www_group }}"
    mode: 0744
    recurse: true
  with_items:
    - "{{ shiny_log_dir }}"
    - "{{ shiny_log_dir }}/apps"

- name: "Check if shiny server is installed"
  stat: path="/usr/bin/shiny-server"
  failed_when: false
  changed_when: false
  register: shiny

- name: "Stop shiny if installed"
  become: yes
  become_method: sudo
  service:
    name: shiny-server
    state: stopped
  when: shiny.stat.islnk is defined

- name: Build and install Shiny-Server (Debian)
  become: true
  become_user: "{{ shiny_user }}"
  become_method: sudo
  tags:
    - install
  include_tasks: install_debian.yml
  notify: shiny start
  when: ansible_distribution == 'Debian'

- name: Install Shiny-Server (CentOS/RHEL)
  become: true
  become_method: sudo
  tags:
    - install
  include_tasks: install_centos.yml
  notify: shiny start
  when: ansible_distribution == 'CentOS' or ansible_distribution == 'Red Hat Enterprise Linux'

- name: Configure Shiny-Server
  tags:
    - install
    - configure
  import_tasks: configure.yml
  notify: shiny restart
